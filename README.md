# pdf2audiobook
This is a project developed during CMPT275 a software engineering course at SFU.
To use this application, please setup your own google cloud account and enable text-to-speech API, Cloud function.
Then generate a service account key for google storage.

This application was designed, analysed, and developed with Aki Zhou (akizhou on Github) and Kirill Melnikov (kmelnikov98 on Github). Without their contribution, this application would not be the application it is today.

## In Action
[![UI.png](https://i.postimg.cc/Qtzn2Z7R/UI.png)](https://postimg.cc/DJqgGDBg)
